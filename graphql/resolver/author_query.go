package resolver

import (
	"context"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	pb "gitlab.com/purwandi/kumparan/graphql/proto"
)

// FindAuthorByID find authors using it's id
func (r *Resolver) FindAuthorByID(ctx context.Context, args struct{ ID string }) (*AuthorResolver, error) {
	authorResolver := &AuthorResolver{}
	id := args.ID
	rpcReq := &pb.AuthorRequest{
		Id: id,
	}
	rpcRes, err := r.AuthorGrpcService.FindAuthorByID(ctx, rpcReq)
	if err != nil {
		return authorResolver, errors.Wrap(err, "find author by id")
	}

	authorResolver.m.Email = rpcRes.Email
	authorResolver.m.Name = rpcRes.Name
	authorResolver.m.ID, err = uuid.FromString(rpcRes.Id)
	if err != nil {
		return authorResolver, errors.Wrap(err, "failed parse id into uuid")
	}

	return authorResolver, nil
}
