package resolver

import (
	"context"
	"log"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/domain"
	pb "gitlab.com/purwandi/kumparan/graphql/proto"
)

// load author from cache
func (r *Resolver) authorFromCache(ctx context.Context, authorID, articleID uuid.UUID) (domain.Author, error) {
	// find author in cache
	authorResult := <-r.AuthorCacheQuery.FindByID(articleID)
	if authorResult.Error != nil {
		return domain.Author{}, errors.Wrap(authorResult.Error, "author cache fault")
	}
	authorRes := authorResult.Result.(pb.AuthorResponse)
	// author exists, load it to article resovler
	if authorRes.Id != "" {
		log.Println("called herer???")
		uid, err := uuid.FromString(authorRes.Id)
		if err != nil {
			return domain.Author{}, errors.Wrap(err, "failed to parse author id")
		}
		author := domain.Author{
			ID:    uid,
			Name:  authorRes.Name,
			Email: authorRes.Email,
		}

		log.Println("165 >>> author from cache", author)
		return author, nil
	}

	// author not exists in cache, Request to author server
	rpcRes, err := r.AuthorGrpcService.FindAuthorByID(ctx, &pb.AuthorRequest{
		Id: authorID.String(),
	})
	if err != nil || rpcRes.Id == "" {
		return domain.Author{}, errors.Wrap(err, "find author by id")
	}
	authorUUID, err := uuid.FromString(rpcRes.Id)
	if err != nil {
		return domain.Author{}, errors.Wrap(err, "failed to parse author id")
	}

	author := domain.Author{
		Name:  rpcRes.Name,
		ID:    authorUUID,
		Email: rpcRes.Email,
	}

	// save the author to cache
	err = <-r.AuthorCacheMutation.Save(rpcRes)
	if err != nil {
		return domain.Author{}, errors.Wrap(err, "failed to save author to cache")
	}

	return author, nil
}

// GetArticles is to get articles
func (r *Resolver) GetArticles(ctx context.Context) (*[]*ArticleResolver, error) {
	rpcRes, err := r.ArticleGrpcService.GetAll(ctx, &empty.Empty{})
	if err != nil {
		return nil, errors.Wrap(err, "cannot connect to rpc")
	}

	if rpcRes == nil {
		return nil, errors.New("no article found")
	}

	result := make([]*ArticleResolver, len(rpcRes.ArticleList))
	for idx, article := range rpcRes.ArticleList {
		articleUUID, err := uuid.FromString(article.Id)
		if err != nil {
			return nil, errors.Wrap(err, "invalid id")
		}
		authorUUID, err := uuid.FromString(article.UserId)
		if err != nil {
			return nil, errors.Wrap(err, "invalid id")
		}

		result[idx] = &ArticleResolver{
			m: domain.Article{
				ID:        articleUUID,
				Name:      article.Name,
				Body:      article.Body,
				CreatedAt: article.CreatedAt,
				UpdatedAt: article.UpdatedAt,
				UserID:    authorUUID,
				Status:    article.Status,
				Slug:      article.Slug,
			},
		}

		author, err := r.authorFromCache(ctx, authorUUID, articleUUID)
		if err != nil {
			return nil, errors.Wrap(err, "can't get author")
		}
		result[idx].m.Author = author
	}

	return &result, nil
}

// FindArticleByID is to find article by id
func (r *Resolver) FindArticleByID(ctx context.Context, args struct{ ID string }) (*ArticleResolver, error) {
	articleResolver := &ArticleResolver{}
	rpcRes, err := r.ArticleGrpcService.FindArticleByID(ctx, &pb.ArticleRequest{
		Id: args.ID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "cannot connect to rpc")
	}

	articleUUID, err := uuid.FromString(rpcRes.Id)
	if err != nil {
		return nil, errors.Wrap(err, "invalid id")
	}

	authorUUID, err := uuid.FromString(rpcRes.UserId)
	if err != nil {
		return nil, errors.Wrap(err, "invalid id")
	}

	articleResolver.m = domain.Article{
		ID:        articleUUID,
		Name:      rpcRes.Name,
		Body:      rpcRes.Body,
		Status:    rpcRes.Status,
		Slug:      rpcRes.Slug,
		UserID:    authorUUID,
		CreatedAt: rpcRes.CreatedAt,
		UpdatedAt: rpcRes.UpdatedAt,
	}

	author, err := r.authorFromCache(ctx, authorUUID, articleUUID)
	if err != nil {
		log.Println("error: ", err)
		return nil, errors.Wrap(err, "can't get author")
	}

	articleResolver.m.Author = author

	return articleResolver, nil
}

// FindArticlesByAuthorID list all articles by authors id
func (r *Resolver) FindArticlesByAuthorID(ctx context.Context, args struct{ AuthorID string }) (*[]*ArticleResolver, error) {
	rpcReq := &pb.ArticleRequest{
		UserId: args.AuthorID,
	}

	rpcRes, err := r.ArticleGrpcService.FindArticlesByAuthorID(ctx, rpcReq)
	if err != nil {
		return nil, errors.Wrap(err, "find article by author id")
	}

	// map []Article to []*ArticleResolver
	articles := rpcRes.ArticleList
	res := make([]*ArticleResolver, 0)
	for _, a := range articles {
		id, err := uuid.FromString(a.Id)
		if err != nil {
			log.Println("error: ", err)
			return nil, errors.Wrap(err, "failed to parse article id")
		}

		authorID, err := uuid.FromString(a.UserId)
		if err != nil {
			log.Println("error: ", err)
			return nil, errors.Wrap(err, "failed to parse article id")
		}

		ar := domain.Article{
			ID:        id,
			Name:      a.Name,
			Body:      a.Body,
			UserID:    authorID,
			Status:    a.Status,
			Slug:      a.Slug,
			CreatedAt: a.CreatedAt,
			UpdatedAt: a.UpdatedAt,
		}

		article := ArticleResolver{m: ar}
		res = append(res, &article)
	}

	return &res, nil
}
