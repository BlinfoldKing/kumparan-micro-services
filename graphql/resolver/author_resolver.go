package resolver

import (
	"context"

	"gitlab.com/purwandi/kumparan/graphql/domain"
)

type AuthorResolver struct {
	m domain.Author
}

func (a *AuthorResolver) ID(ctx context.Context) *string {
	id := a.m.ID.String()
	return &id
}

func (a *AuthorResolver) Name(ctx context.Context) *string {
	return &a.m.Name
}

func (a *AuthorResolver) Email(ctx context.Context) *string {
	return &a.m.Email
}
