package resolver

import (
	"context"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/domain"
	pb "gitlab.com/purwandi/kumparan/graphql/proto"
)

// ArticleInput input result
type ArticleInput struct {
	Name   *string
	Body   *string
	UserID *string
}

// CreateArticle to create
func (r *Resolver) CreateArticle(ctx context.Context, args struct{ Article ArticleInput }) (*ArticleResolver, error) {
	articleResolver := &ArticleResolver{}
	rpcReq := &pb.ArticleRequest{
		Name:   *args.Article.Name,
		Body:   *args.Article.Body,
		UserId: *args.Article.UserID,
	}
	rpcRes, err := r.ArticleGrpcService.CreateArticle(ctx, rpcReq)
	if err != nil {
		return nil, errors.Wrap(err, "cannot connect to rpc")
	}

	uid, err := uuid.FromString(rpcRes.Id)
	if err != nil {
		return nil, errors.Wrap(err, "invalid id")
	}

	articleResolver.m = domain.Article{
		ID:        uid,
		Name:      rpcRes.Name,
		Body:      rpcRes.Body,
		Status:    rpcRes.Status,
		Slug:      rpcRes.Slug,
		CreatedAt: rpcRes.CreatedAt,
		UpdatedAt: rpcRes.UpdatedAt,
	}

	return articleResolver, nil
}
