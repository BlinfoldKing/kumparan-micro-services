package resolver

import (
	"context"
	"fmt"

	"gitlab.com/purwandi/kumparan/graphql/domain"
)

// ArticleResolver is to resolve article
type ArticleResolver struct {
	m domain.Article
}

// ID to resolve id
func (r *ArticleResolver) ID(ctx context.Context) *string {
	result := r.m.ID.String()
	return &result
}

// Name is to resolve
func (r *ArticleResolver) Name(ctx context.Context) *string {
	return &r.m.Name
}

// Slug is to resolve slug name
func (r *ArticleResolver) Slug(ctx context.Context) *string {
	return &r.m.Slug
}

// Body is to resolve body
func (r *ArticleResolver) Body(ctx context.Context) *string {
	return &r.m.Body
}

// Status is to resolve status
func (r *ArticleResolver) Status(ctx context.Context) *string {
	return &r.m.Status
}

// CreatedAt is to  created
func (r *ArticleResolver) CreatedAt(ctx context.Context) *string {
	result := fmt.Sprint(r.m.CreatedAt)
	return &result
}

// UpdatedAt is
func (r *ArticleResolver) UpdatedAt(ctx context.Context) *string {
	result := fmt.Sprint(r.m.UpdatedAt)
	return &result
}

// Author author's of the article
func (r *ArticleResolver) Author(ctx context.Context) *AuthorResolver {
	// assign article author to author resolver
	authorResolver := AuthorResolver{m: r.m.Author}

	return &authorResolver
}
