package resolver

import (
	"context"

	"gitlab.com/purwandi/kumparan/graphql/domain"

	pb "gitlab.com/purwandi/kumparan/graphql/proto"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

// AuthorInput input for CreateAuthor
type AuthorInput struct {
	Name  *string
	Email *string
}

// CreateAuthor create new author
func (r *Resolver) CreateAuthor(ctx context.Context, args struct{ Author AuthorInput }) (*AuthorResolver, error) {
	email := *args.Author.Email
	name := *args.Author.Name
	rpcReq := &pb.AuthorRequest{
		Name:  name,
		Email: email,
	}
	res, err := r.AuthorGrpcService.CreateAuthor(ctx, rpcReq)
	if err != nil {
		return nil, errors.Wrap(err, "save author")
	}

	uid, err := uuid.FromString(res.Id)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse")
	}
	author := domain.Author{
		Email: email,
		Name:  name,
		ID:    uid,
	}

	authorResolver := &AuthorResolver{m: author}
	return authorResolver, nil
}
