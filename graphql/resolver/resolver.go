package resolver

import (
	"gitlab.com/purwandi/kumparan/graphql/proto"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/services"
)

// Resolver is to resolve
type Resolver struct {
	// Ganti koneksi ke grpc
	// ------------------------------------------
	ArticleService     *services.ArticleService
	ArticleGrpcService proto.ArticleServiceClient
	AuthorService      services.AuthorService
	AuthorGrpcService  proto.AuthorServiceClient

	AuthorCacheQuery    repository.AuthorCacheQuery
	AuthorCacheMutation repository.AuthorCacheMutation
}
