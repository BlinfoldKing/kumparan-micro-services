package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/purwandi/kumparan/graphql/container"
	"gitlab.com/purwandi/kumparan/graphql/storage"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	log "github.com/sirupsen/logrus"
	"gitlab.com/purwandi/kumparan/graphql/repository/inmemory"
	"gitlab.com/purwandi/kumparan/graphql/resolver"
	"gitlab.com/purwandi/kumparan/graphql/services"
)

type query struct{}

func (*query) Hello() string {
	return "Hello, world!"
}

func main() {
	appContainer := container.Container{}
	cfg := appContainer.Config()
	s, err := getSchema("./schema/schema.graphql")
	if err != nil {
		panic(err)
	}

	articleService := services.NewArticleService()
	authorStorage := storage.NewAuthorStorage()
	authorRepo := inmemory.NewAuthorRepositoryInMemory(authorStorage)
	authorQuery := inmemory.NewAuthorQueryInMemory(authorStorage)
	authorService := services.NewAuthorService(authorRepo, authorQuery)
	resolver := &resolver.Resolver{
		ArticleService:      articleService,
		AuthorService:       authorService,
		AuthorGrpcService:   appContainer.AuthorServiceClient(),
		ArticleGrpcService:  appContainer.ArticleServiceClient(),
		AuthorCacheQuery:    appContainer.AuthorCacheQuery(),
		AuthorCacheMutation: appContainer.AuthorCacheMutation(),
	}
	schema := graphql.MustParseSchema(s, resolver)

	mux := http.NewServeMux()

	mux.Handle("/query", &relay.Handler{Schema: schema})

	log.Printf("server listening on :%d\n", cfg.Port)

	log.WithFields(log.Fields{"time": time.Now()}).Info("starting server")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), logged(mux)))
}

func getSchema(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

// logging middleware
func logged(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now().UTC()

		next.ServeHTTP(w, r)

		log.WithFields(log.Fields{
			"path":    r.RequestURI,
			"IP":      r.RemoteAddr,
			"elapsed": time.Now().UTC().Sub(start),
		}).Info()
	})
}
