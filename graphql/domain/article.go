package domain

import (
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
)

const (
	// PUBLISHED if the article is published
	PUBLISHED string = "PUBLISHED"
	// DRAFT if the article is draft
	DRAFT string = "DRAFT"
)

// Article is to wrap article
type Article struct {
	ID        uuid.UUID `json:"id"`
	Slug      string    `json:"slug"`
	Name      string    `json:"name"`
	Body      string    `json:"body"`
	Status    string    `json:"status"`
	UserID    uuid.UUID `json:"user_id"`
	CreatedAt int64     `json:"created_at"`
	UpdatedAt int64     `json:"updated_at"`
	Author    Author    `json:"author"`
}

// CreateArticle is to create article instance
func CreateArticle(name, body string) (*Article, error) {
	uid, err := uuid.NewV4()
	if err != nil {
		return &Article{}, err
	}

	slug := GenerateSlug(name)
	now := time.Now()
	return &Article{
		ID:        uid,
		Name:      name,
		Slug:      slug,
		Body:      body,
		Status:    DRAFT,
		CreatedAt: now.UnixNano(),
		UpdatedAt: now.UnixNano(),
	}, nil

}

// GenerateSlug is generate slug article based on name
func GenerateSlug(name string) string {
	slug := strings.TrimSpace(name)
	slug = strings.ReplaceAll(slug, " ", "-")

	return strings.ToLower(slug)
}

// AttachUserID for given article
func (article *Article) AttachUserID(id uuid.UUID) error {
	article.UserID = id
	article.UpdatedAt = time.Now().UnixNano()

	return nil
}
