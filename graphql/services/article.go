package services

import (
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/domain"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/repository/inmemory"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

// ArticleService is
type ArticleService struct {
	Query      repository.ArticleQuery
	Repository repository.ArticleRepository
}

// NewArticleService is to create article services
func NewArticleService() *ArticleService {
	var repo repository.ArticleRepository
	var query repository.ArticleQuery

	db := storage.NewArticleStorage()
	query = inmemory.NewArticleQueryInMemory(db)
	repo = inmemory.NewArticleRepositoryInMemory(db)

	return &ArticleService{
		Query:      query,
		Repository: repo,
	}
}

// GetAllArticles is to find all articles
func (service *ArticleService) GetAllArticles() ([]domain.Article, error) {
	// Process
	result := <-service.Query.GetArticles()
	if result.Error != nil {
		return []domain.Article{}, result.Error
	}

	// Return
	return result.Result.([]domain.Article), nil
}

// FindArticleByID is to find article by id
func (service *ArticleService) FindArticleByID(uid string) (domain.Article, error) {
	id, err := uuid.FromString(uid)
	if err != nil {
		return domain.Article{}, err
	}

	result := <-service.Query.FindArticleByID(id)
	if result.Error != nil {
		// log.WithFields(log.Fields{"time": time.Now()}).Info("Ini error dari services")
		return domain.Article{}, result.Error
	}

	// log.WithFields(log.Fields{"time": time.Now()}).Info("No error")

	return result.Result.(domain.Article), nil
}

// CreateArticle to create article
func (service *ArticleService) CreateArticle(name, body string) (domain.Article, error) {
	// Process
	result, err := domain.CreateArticle(name, body)
	if err != nil {
		return domain.Article{}, err
	}

	// Persist
	err = <-service.Repository.Save(result)
	if err != nil {
		return domain.Article{}, err
	}

	return *result, nil
}

// AddAuthor add an author id to existing article
func (service *ArticleService) AddAuthor(articleID, userID uuid.UUID) error {
	err := <-service.Repository.AddAuthor(articleID, userID)
	if err != nil {
		return errors.Wrap(err, "add author")
	}

	return nil
}

// FindArticlesByAuthorID list articles of author by it's id
func (service *ArticleService) FindArticlesByAuthorID(authorID uuid.UUID) ([]domain.Article, error) {
	res := <-service.Query.FindArticlesByAuthorID(authorID)
	if res.Error != nil {
		return nil, errors.Wrap(res.Error, "query find articles by author's id")
	}

	return res.Result.([]domain.Article), nil
}
