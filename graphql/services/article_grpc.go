package services

import (
	"github.com/pkg/errors"
	"gitlab.com/purwandi/kumparan/graphql/config"
	pb "gitlab.com/purwandi/kumparan/graphql/proto"
	"google.golang.org/grpc"
)

// NewAuthorServiceClient return AuthorServiceClient connection
func NewArticleServiceClient(cfg config.Config) (pb.ArticleServiceClient, error) {
	var conn *grpc.ClientConn
	var err error

	if cfg.Mode == config.DevMode {
		conn, err = grpc.Dial(cfg.GrpcArticleAddr, grpc.WithInsecure())
	} else {
		conn, err = grpc.Dial(cfg.GrpcArticleAddr)
	}

	if err != nil {
		return nil, errors.Wrap(err, "failed to dial")
	}

	client := pb.NewArticleServiceClient(conn)
	return client, nil
}
