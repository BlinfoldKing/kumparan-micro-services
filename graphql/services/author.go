package services

import (
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/domain"
	"gitlab.com/purwandi/kumparan/graphql/repository"
)

type AuthorService interface {
	Save(name, email string) (domain.Author, error)
	FindByID(uuid uuid.UUID) (domain.Author, error)
}

type authorService struct {
	Repository repository.AuthorRepository
	Query      repository.AuthorQuery
}

func NewAuthorService(repository repository.AuthorRepository, query repository.AuthorQuery) AuthorService {
	return &authorService{
		Repository: repository,
		Query:      query,
	}
}

func (as *authorService) FindByID(uuid uuid.UUID) (domain.Author, error) {
	result := <-as.Query.FindByID(uuid)
	if result.Error != nil {
		return domain.Author{}, errors.Wrap(result.Error, "FindByID")
	}

	return result.Result.(domain.Author), nil
}

func (as *authorService) Save(name, email string) (domain.Author, error) {
	author, err := domain.CreateAuthor(name, email)
	if err != nil {
		return *author, errors.Wrap(err, "CreateAuthor")
	}

	err = <-as.Repository.Save(author)
	if err != nil {
		return *author, errors.Wrap(err, "Repository.Save")
	}

	return *author, nil
}
