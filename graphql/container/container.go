package container

import (
	"log"

	"gitlab.com/purwandi/kumparan/graphql/config"
	pb "gitlab.com/purwandi/kumparan/graphql/proto"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/repository/inmemory"
	"gitlab.com/purwandi/kumparan/graphql/services"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

// Container contains dependencies
type Container struct {
	cfg *config.Config

	authorServiceClient  pb.AuthorServiceClient
	articleServiceClient pb.ArticleServiceClient
	inememCache          *storage.InMemoryCache

	authorCacheMutation repository.AuthorCacheMutation
	authorCacheQuery    repository.AuthorCacheQuery
}

// Config return app config
func (c *Container) Config() config.Config {
	if c.cfg == nil {
		c.cfg = &config.Config{}
		c.cfg.Load()
	}

	return *c.cfg
}

// AuthorServiceClient return author service client connection
func (c *Container) AuthorServiceClient() pb.AuthorServiceClient {
	var err error
	defer recover()
	if c.authorServiceClient == nil {
		c.authorServiceClient, err = services.NewAuthorServiceClient(c.Config())
		if err != nil {
			log.Println("error: ", err)
			panic(err)
		}
	}

	return c.authorServiceClient
}

// AuthorServiceClient return author service client connection
func (c *Container) ArticleServiceClient() pb.ArticleServiceClient {
	var err error
	defer recover()
	if c.articleServiceClient == nil {
		c.articleServiceClient, err = services.NewArticleServiceClient(c.Config())
		if err != nil {
			panic(err)
		}
	}

	return c.articleServiceClient
}

// InMemCache return in memmory cache
func (c *Container) InMemCache() *storage.InMemoryCache {
	if c.inememCache == nil {
		c.inememCache = storage.NewInMemoryCache()
	}

	return c.inememCache
}

// AuthorCacheMutation return implementation of author cache mutation
func (c *Container) AuthorCacheMutation() repository.AuthorCacheMutation {
	if c.authorCacheMutation == nil {
		c.authorCacheMutation = inmemory.NewAuthorCacheInMemMutation(c.InMemCache())
	}

	return c.authorCacheMutation
}

// AuthorCacheQuery return implementation of author cache query
func (c *Container) AuthorCacheQuery() repository.AuthorCacheQuery {
	if c.authorCacheQuery == nil {
		c.authorCacheQuery = inmemory.NewAuthorCacheInMemQuery(c.InMemCache())
	}

	return c.authorCacheQuery
}
