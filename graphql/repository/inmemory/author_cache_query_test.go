package inmemory

import (
	"testing"

	"gitlab.com/purwandi/kumparan/graphql/repository"

	"github.com/stretchr/testify/assert"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

func TestAuthorCacheQuery_FindByID(t *testing.T) {
	t.Run("success get author", func(t *testing.T) {
		cache := storage.NewInMemoryCache()
		cache.Authors = manyAuthors
		query := NewAuthorCacheInMemQuery(*cache)
		got := repository.QueryResult{}
		want := manyAuthors["00000000-0000-0000-0000-000000000001"]

		done := make(chan bool)
		go func() {
			id, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
			got = <-query.FindByID(id)
			done <- true
		}()
		<-done

		assert.Equal(t, want, got.Result)
	})
}
