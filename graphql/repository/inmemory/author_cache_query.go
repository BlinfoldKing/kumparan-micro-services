package inmemory

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/proto"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

type authorCacheQuery struct {
	cache *storage.InMemoryCache
}

// NewAuthorCacheInMemQuery return authorCacheQuery in memmory
func NewAuthorCacheInMemQuery(cache *storage.InMemoryCache) repository.AuthorCacheQuery {
	return &authorCacheQuery{cache: cache}
}

func (a *authorCacheQuery) FindByID(id uuid.UUID) <-chan repository.QueryResult {
	res := make(chan repository.QueryResult)

	go func() {
		author, ok := a.cache.Authors[id.String()]
		if !ok {
			res <- repository.QueryResult{Result: proto.AuthorResponse{}}
			return
		}

		res <- repository.QueryResult{Result: author}
	}()

	return res
}
