package inmemory

import (
	"gitlab.com/purwandi/kumparan/graphql/domain"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

type AuthorRepositoryInMemory struct {
	Storage *storage.AuthorStorage
}

func NewAuthorRepositoryInMemory(storage *storage.AuthorStorage) repository.AuthorRepository {
	return &AuthorRepositoryInMemory{
		storage,
	}
}

func (ar *AuthorRepositoryInMemory) Save(author *domain.Author) <-chan error {
	result := make(chan error)

	go func() {
		ar.Storage.AuthorMap = append(ar.Storage.AuthorMap, *author)
		result <- nil
		close(result)
	}()

	return result
}
