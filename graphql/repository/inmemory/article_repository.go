package inmemory

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/domain"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

// ArticleRepositoryInMemory is article repository implementation in memory
type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleRepositoryInMemory is to create ArticleRepositoryInMemory instance
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) repository.ArticleRepository {
	return &ArticleRepositoryInMemory{Storage: storage}
}

// Save is for save article
func (repo *ArticleRepositoryInMemory) Save(article *domain.Article) <-chan error {
	result := make(chan error)

	go func() {
		repo.Storage.ArticleMap = append(repo.Storage.ArticleMap, *article)

		result <- nil
		close(result)
	}()

	return result
}

func (repo *ArticleRepositoryInMemory) AddAuthor(articleID, authorID uuid.UUID) <-chan error {
	err := make(chan error)
	go func() {
		for i, a := range repo.Storage.ArticleMap {
			if a.ID == articleID {
				repo.Storage.ArticleMap[i].UserID = authorID
				err <- nil
				close(err)
				break
			}
		}
	}()

	return err
}
