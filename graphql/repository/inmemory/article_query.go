package inmemory

import (
	"errors"
	"log"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/domain"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

// ArticleQueryInMemory is article query implementation in memory
type ArticleQueryInMemory struct {
	Storage *storage.ArticleStorage
}

// NewArticleQueryInMemory is to Create Instance ArticleQueryInMemory
func NewArticleQueryInMemory(storage *storage.ArticleStorage) repository.ArticleQuery {
	return &ArticleQueryInMemory{Storage: storage}
}

// GetArticles is to find published article by slug
func (query *ArticleQueryInMemory) GetArticles() <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		result <- repository.QueryResult{
			Result: query.Storage.ArticleMap,
		}
		close(result)
	}()

	return result
}

// FindArticleByID is to find article by id
func (query *ArticleQueryInMemory) FindArticleByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		article := domain.Article{}
		for _, item := range query.Storage.ArticleMap {
			if id == item.ID {
				article = item
			}
		}

		if article.Name == "" {
			result <- repository.QueryResult{Error: errors.New("Article not found")}
		} else {
			result <- repository.QueryResult{Result: article}
		}

		close(result)
	}()

	return result
}

// FindArticlesByAuthorID query all articles of author by author's id
func (query *ArticleQueryInMemory) FindArticlesByAuthorID(authorID uuid.UUID) <-chan repository.QueryResult {
	res := make(chan repository.QueryResult)
	go func() {
		articles := make([]domain.Article, 0)
		for _, article := range query.Storage.ArticleMap {
			log.Println("query article", article.Name, article.UserID)

			if article.UserID == authorID {
				articles = append(articles, article)
			}
		}

		res <- repository.QueryResult{Result: articles, Error: nil}
		log.Println("articles ", articles)
		close(res)
	}()

	return res
}
