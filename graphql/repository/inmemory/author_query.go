package inmemory

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

type AuthorQueryInMemory struct {
	Storage *storage.AuthorStorage
}

func NewAuthorQueryInMemory(storage *storage.AuthorStorage) repository.AuthorQuery {
	return &AuthorQueryInMemory{
		storage,
	}
}

// TODO: fix, still bug if author is not exists in storage
func (a *AuthorQueryInMemory) FindByID(uuid uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		for _, author := range a.Storage.AuthorMap {
			if author.ID == uuid {
				result <- repository.QueryResult{author, nil}
				close(result)
				break
			}

		}
	}()

	return result
}
