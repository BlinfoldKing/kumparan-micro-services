package inmemory

import (
	"log"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"

	pb "gitlab.com/purwandi/kumparan/graphql/proto"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

var manyAuthors = map[string]pb.AuthorResponse{
	"00000000-0000-0000-0000-000000000001": pb.AuthorResponse{
		Id:    "00000000-0000-0000-0000-000000000001",
		Name:  "John",
		Email: "john@doe.com",
	},
	"00000000-0000-0000-0000-000000000002": pb.AuthorResponse{
		Id:    "00000000-0000-0000-0000-000000000002",
		Name:  "Jean",
		Email: "jean@doe.com",
	},
	"00000000-0000-0000-0000-000000000003": pb.AuthorResponse{
		Id:    "00000000-0000-0000-0000-000000000003",
		Name:  "jack",
		Email: "jack@doe.com",
	},
}

func TestAuthorCacheMutation_Save(t *testing.T) {
	t.Run("save author to cache when cahce empty", func(t *testing.T) {
		cache := storage.NewInMemoryCache()
		mutation := NewAuthorCacheInMemMutation(cache)
		author := manyAuthors["00000000-0000-0000-0000-000000000001"]
		wg := sync.WaitGroup{}
		wg.Add(1)

		var err error
		go func() {
			err = <-mutation.Save(&author)
			wg.Done()
		}()

		assert.NoError(t, err)
	})

	t.Run("append author to cache, when cache not empty", func(t *testing.T) {
		cache := storage.NewInMemoryCache()
		mutation := NewAuthorCacheInMemMutation(cache)
		cache.Authors = manyAuthors
		assert.Equal(t, 3, len(cache.Authors))

		done := make(chan bool)
		var err error
		author := pb.AuthorResponse{
			Id:    "00000000-0000-0000-0000-000000000004",
			Name:  "han",
			Email: "han@doe.com",
		}

		go func() {
			err = <-mutation.Save(&author)
			done <- true
		}()
		<-done

		log.Println(cache)

		assert.NoError(t, err)
		assert.Equal(t, 4, len(cache.Authors))
	})
}
