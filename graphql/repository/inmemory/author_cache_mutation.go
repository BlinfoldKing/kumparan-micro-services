package inmemory

import (
	"gitlab.com/purwandi/kumparan/graphql/proto"
	"gitlab.com/purwandi/kumparan/graphql/repository"
	"gitlab.com/purwandi/kumparan/graphql/storage"
)

type authorCacheMutation struct {
	cache *storage.InMemoryCache
}

// NewAuthorCacheInMemMutation return authorCacheMutation
func NewAuthorCacheInMemMutation(cache *storage.InMemoryCache) repository.AuthorCacheMutation {
	return &authorCacheMutation{cache}
}

func (a *authorCacheMutation) Save(author *proto.AuthorResponse) <-chan error {
	err := make(chan error)
	go func() {
		a.cache.Authors[author.Id] = *author
		err <- nil
	}()

	return err
}
