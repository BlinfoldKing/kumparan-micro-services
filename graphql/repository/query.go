package repository

import uuid "github.com/satori/go.uuid"

// QueryResult is to wrap query result
type QueryResult struct {
	Result interface{}
	Error  error
}

// ArticleQuery is contract for article query
type ArticleQuery interface {
	// single
	GetArticles() <-chan QueryResult
	FindArticleByID(uuid.UUID) <-chan QueryResult
	FindArticlesByAuthorID(authorID uuid.UUID) <-chan QueryResult
}

type AuthorQuery interface {
	// single
	FindByID(uuid.UUID) <-chan QueryResult
}

type AuthorCacheQuery interface {
	FindByID(uuid.UUID) <-chan QueryResult
}
