package repository

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/purwandi/kumparan/graphql/domain"
	"gitlab.com/purwandi/kumparan/graphql/proto"
)

// ArticleRepository is wrap contract article repository
type ArticleRepository interface {
	Save(article *domain.Article) <-chan error
	AddAuthor(articleID, authorID uuid.UUID) <-chan error
}

// AuthorRepository is wrap contract article repository
type AuthorRepository interface {
	Save(author *domain.Author) <-chan error
}

// AuthorCacheMutation save author to cache
type AuthorCacheMutation interface {
	Save(author *proto.AuthorResponse) <-chan error
}
