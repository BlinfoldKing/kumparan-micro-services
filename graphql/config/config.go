package config

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

// env
// mode: dev, prod, test
const (
	DevMode         string = "dev"
	Mode                   = "MODE"
	Port                   = "PORT"
	GrpcAuthorPort         = "GRPC_AUTHOR_PORT"
	GrpcAuthorName         = "GRPC_AUTHOR_NAME"
	GrpcArticlePort        = "GRPC_ARTICLE_PORT"
	GrpcArticleName        = "GRPC_ARTICLE_NAME"
)

// Config app configuration
type Config struct {
	Port            int
	GrpcAuthorPort  int
	GrpcAuthorAddr  string
	GrpcArticlePort int
	GrpcArticleAddr string
	Mode            string
}

func init() {

}

// Load load configuration
func (c *Config) Load() {

	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}

	// default value
	c.Mode = DevMode
	c.Port = 9000
	c.GrpcAuthorPort = 3000
	c.GrpcAuthorAddr = fmt.Sprintf("localhost:%d", c.GrpcAuthorPort)
	c.GrpcArticlePort = 4000
	c.GrpcArticleAddr = fmt.Sprintf("localhost:%d", c.GrpcArticlePort)

	if os.Getenv(GrpcAuthorPort) != "" {
		c.GrpcAuthorPort, err = strconv.Atoi(os.Getenv(GrpcAuthorPort))
		if err != nil {
			log.Fatal("cannot parse: ", GrpcAuthorPort, err)
		}
	}

	if os.Getenv(Mode) != "" {
		c.Mode = os.Getenv("MODE")
	}

	if os.Getenv(GrpcAuthorName) != "" {
		c.GrpcAuthorAddr = fmt.Sprintf("%s:%d", os.Getenv(GrpcAuthorName), c.GrpcAuthorPort)
	}

	if os.Getenv(Port) != "" {
		c.Port, err = strconv.Atoi(os.Getenv("PORT"))
		if err != nil {
			log.Fatal("cannot parse PORT", err)
		}
	}

	if os.Getenv(GrpcArticleName) != "" {
		c.GrpcArticleAddr = fmt.Sprintf("%s:%d", os.Getenv(GrpcArticleName), c.GrpcArticlePort)
	}
}
