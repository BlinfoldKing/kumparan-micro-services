package storage

import (
	"gitlab.com/purwandi/kumparan/graphql/proto"
)

// InMemoryCache cache
type InMemoryCache struct {
	// map author id to author
	Authors map[string]proto.AuthorResponse
}

// NewInMemoryCache return InMemoryCache
func NewInMemoryCache() *InMemoryCache {
	return &InMemoryCache{
		Authors: make(map[string]proto.AuthorResponse),
	}
}
