package storage

import "gitlab.com/purwandi/kumparan/graphql/domain"

// ArticleStorage is article storage in memory
type ArticleStorage struct {
	ArticleMap []domain.Article
}

// NewArticleStorage is to create article storage
func NewArticleStorage() *ArticleStorage {
	// article1, _ := domain.CreateArticle("Pelita Harapan", "Ini pelita harapan body")
	// article2, _ := domain.CreateArticle("Jantung Harapan", "Ini jantung harapan body")

	return &ArticleStorage{
		ArticleMap: []domain.Article{
			// *article1, *article2,
		},
	}
}

// AuthorStorage is to create author
type AuthorStorage struct {
	AuthorMap []domain.Author
}

// NewAuthorStorage s
func NewAuthorStorage() *AuthorStorage {
	return &AuthorStorage{}
}
