compose:
	@cd graphql/ && make build-prod
	@cd author/ && make build-prod
	@cd article/ && make build
	@docker-compose build
	@docker-compose up
