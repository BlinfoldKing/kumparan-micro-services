package inmemory

import (
	"testing"

	"article-service/domain"
	"article-service/storage"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanSaveArticleInRepository(t *testing.T) {
	// Given
	done := make(chan bool)
	articleUID := uuid.NewV4()
	article := domain.Article{
		ID:     articleUID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	db := storage.NewArticleStorage()
	repo := NewArticleRepositoryInMemory(db)

	// When
	var err error
	go func() {
		err = <-repo.Save(&article)
		done <- true
	}()

	<-done

	// Then
	assert.Nil(t, err)
	assert.Equal(t, 1, len(db.ArticleMap))
}

func TestCanAddAuthor(t *testing.T) {
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1 := domain.Article{
		ID:     article1UID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1,
		},
	}

	repo := NewArticleRepositoryInMemory(&storage)

	expectedUserID := uuid.NewV4()

	// when
	var err error
	go func() {
		err = <-repo.AddAuthor(article1.ID, expectedUserID)
		done <- true
	}()

	<-done

	// Then
	assert.Empty(t, err)
}
