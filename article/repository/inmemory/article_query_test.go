package inmemory

import (
	"testing"

	"article-service/domain"
	"article-service/repository"
	"article-service/storage"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestCanGetAllArticle(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1 := domain.Article{
		ID:     article1UID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	article2UID := uuid.NewV4()
	article2 := domain.Article{
		ID:     article2UID,
		Name:   "Foobar 2",
		Slug:   "foobar-2",
		Body:   "body article 2",
		Status: domain.PUBLISHED,
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1, article2,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.GetArticles()
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, storage.ArticleMap, result.Result.([]domain.Article))
}

func TestCanFindArticleByID(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1 := domain.Article{
		ID:     article1UID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	article2UID := uuid.NewV4()
	article2 := domain.Article{
		ID:     article2UID,
		Name:   "Foobar 2",
		Slug:   "foobar-2",
		Body:   "body article 2",
		Status: domain.PUBLISHED,
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1, article2,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindArticleByID(article1.ID)
		done <- true
	}()

	<-done

	// Then
	assert.Equal(t, article1, result.Result.(domain.Article))
}

func TestCantFindArticleByID(t *testing.T) {
	// Given
	done := make(chan bool)

	article1UID := uuid.NewV4()
	article1 := domain.Article{
		ID:     article1UID,
		Name:   "Foobar",
		Slug:   "foobar",
		Body:   "body article",
		Status: domain.PUBLISHED,
	}

	article2UID := uuid.NewV4()
	article2 := domain.Article{
		ID:     article2UID,
		Name:   "Foobar 2",
		Slug:   "foobar-2",
		Body:   "body article 2",
		Status: domain.PUBLISHED,
	}

	storage := storage.ArticleStorage{
		ArticleMap: []domain.Article{
			article1,
		},
	}

	query := NewArticleQueryInMemory(&storage)
	result := repository.QueryResult{}

	// When
	go func() {
		result = <-query.FindArticleByID(article2.ID)
		done <- true
	}()

	<-done

	// Then
	assert.Error(t, result.Error)
}

var articleID1, _ = uuid.FromString("00000000-0000-0000-0000-000000000001")
var articleID2, _ = uuid.FromString("00000000-0000-0000-0000-000000000002")
var articleID3, _ = uuid.FromString("00000000-0000-0000-0000-000000000003")

var manyArticles = []domain.Article{
	domain.Article{ID: articleID1, Name: "article 1", Slug: "slug-article-1", UserID: articleID1},
	domain.Article{ID: articleID3, Name: "article 3", Slug: "slug-article-3", UserID: articleID1},
	domain.Article{ID: articleID2, Name: "article 2", Slug: "slug-article-2", UserID: articleID2},
}

func TestArticleQuery_FindArticlesByAuthorID(t *testing.T) {
	store := storage.NewArticleStorage()
	store.ArticleMap = manyArticles
	t.Run("return empty articles, when user not found", func(t *testing.T) {
		notExistsID, _ := uuid.FromString("00000000-0000-0000-0000-000000000099")
		query := NewArticleQueryInMemory(store)

		done := make(chan bool)
		got := make([]domain.Article, 0)
		go func() {
			res := query.FindArticlesByAuthorID(notExistsID)
			queryRes := <-res
			got = queryRes.Result.([]domain.Article)
			done <- true
		}()
		<-done

		assert.Equal(t, 0, len(got))
	})

	t.Run("return articles, when user found", func(t *testing.T) {
		existsID := articleID1
		query := NewArticleQueryInMemory(store)

		done := make(chan bool)
		got := make([]domain.Article, 0)
		go func() {
			res := query.FindArticlesByAuthorID(existsID)
			queryRes := <-res
			got = queryRes.Result.([]domain.Article)
			done <- true
		}()
		<-done

		want := manyArticles[:2]

		assert.Equal(t, 2, len(got))
		assert.Equal(t, want, got)
	})
}
