package repository

import (
	"article-service/domain"

	uuid "github.com/satori/go.uuid"
)

// ArticleRepository is wrap contract article repository
type ArticleRepository interface {
	Save(article *domain.Article) <-chan error
	AddAuthor(articleID, authorID uuid.UUID) <-chan error
}
