package client

import (
	pb "article-service/proto"
	"log"

	"google.golang.org/grpc"
)

func ConnectAuthorServer(port string) pb.AuthorServiceClient {
	conn, err := grpc.Dial(port, grpc.WithInsecure())
	if err != nil {
		log.Fatal("ERR: ", port, err)
	}

	return pb.NewAuthorServiceClient(conn)
}
