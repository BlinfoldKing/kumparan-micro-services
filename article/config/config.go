package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	AuthorAddr  string
	ArticleAddr string
}

func Load(mode string) Config {
	if mode == "dev" {
		err := godotenv.Load(".env")
		if err != nil {
			log.Println(err)
		}
	}

	authorAddr := os.Getenv("AUTHOR_SERVICE_ADDR")
	articleAddr := os.Getenv("ARTICLE_SERVICE_ADDR")

	if authorAddr == "" {
		authorAddr = ":3000"
	}
	if articleAddr == "" {
		articleAddr = ":4000"
	}

	return Config{
		AuthorAddr:  authorAddr,
		ArticleAddr: articleAddr,
	}
}
