# Article Service

## Running Instruction
1. create a `.env` file with key such as `.env.example`
2. to run:
```

# .env needed
go run . dev

# or just do 
go run . # will directly read env var of your os, without .env

```

PS: if no env var found, the sever will set it to default value
