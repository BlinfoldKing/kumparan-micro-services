// Code generated by protoc-gen-go. DO NOT EDIT.
// source: Author.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type AuthorResponse struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Email                string   `protobuf:"bytes,3,opt,name=email,proto3" json:"email,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AuthorResponse) Reset()         { *m = AuthorResponse{} }
func (m *AuthorResponse) String() string { return proto.CompactTextString(m) }
func (*AuthorResponse) ProtoMessage()    {}
func (*AuthorResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_c55d337120169738, []int{0}
}

func (m *AuthorResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AuthorResponse.Unmarshal(m, b)
}
func (m *AuthorResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AuthorResponse.Marshal(b, m, deterministic)
}
func (m *AuthorResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AuthorResponse.Merge(m, src)
}
func (m *AuthorResponse) XXX_Size() int {
	return xxx_messageInfo_AuthorResponse.Size(m)
}
func (m *AuthorResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_AuthorResponse.DiscardUnknown(m)
}

var xxx_messageInfo_AuthorResponse proto.InternalMessageInfo

func (m *AuthorResponse) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *AuthorResponse) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *AuthorResponse) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

type AuthorRequest struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Email                string   `protobuf:"bytes,2,opt,name=email,proto3" json:"email,omitempty"`
	Id                   string   `protobuf:"bytes,3,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AuthorRequest) Reset()         { *m = AuthorRequest{} }
func (m *AuthorRequest) String() string { return proto.CompactTextString(m) }
func (*AuthorRequest) ProtoMessage()    {}
func (*AuthorRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_c55d337120169738, []int{1}
}

func (m *AuthorRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AuthorRequest.Unmarshal(m, b)
}
func (m *AuthorRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AuthorRequest.Marshal(b, m, deterministic)
}
func (m *AuthorRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AuthorRequest.Merge(m, src)
}
func (m *AuthorRequest) XXX_Size() int {
	return xxx_messageInfo_AuthorRequest.Size(m)
}
func (m *AuthorRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_AuthorRequest.DiscardUnknown(m)
}

var xxx_messageInfo_AuthorRequest proto.InternalMessageInfo

func (m *AuthorRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *AuthorRequest) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *AuthorRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func init() {
	proto.RegisterType((*AuthorResponse)(nil), "proto.AuthorResponse")
	proto.RegisterType((*AuthorRequest)(nil), "proto.AuthorRequest")
}

func init() { proto.RegisterFile("Author.proto", fileDescriptor_c55d337120169738) }

var fileDescriptor_c55d337120169738 = []byte{
	// 186 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x71, 0x2c, 0x2d, 0xc9,
	0xc8, 0x2f, 0xd2, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x05, 0x53, 0x4a, 0x5e, 0x5c, 0x7c,
	0x10, 0xe1, 0xa0, 0xd4, 0xe2, 0x82, 0xfc, 0xbc, 0xe2, 0x54, 0x21, 0x3e, 0x2e, 0xa6, 0xcc, 0x14,
	0x09, 0x46, 0x05, 0x46, 0x0d, 0xce, 0x20, 0xa6, 0xcc, 0x14, 0x21, 0x21, 0x2e, 0x96, 0xbc, 0xc4,
	0xdc, 0x54, 0x09, 0x26, 0xb0, 0x08, 0x98, 0x2d, 0x24, 0xc2, 0xc5, 0x9a, 0x9a, 0x9b, 0x98, 0x99,
	0x23, 0xc1, 0x0c, 0x16, 0x84, 0x70, 0x94, 0x3c, 0xb9, 0x78, 0x61, 0x66, 0x15, 0x96, 0xa6, 0x16,
	0x97, 0xc0, 0xb5, 0x32, 0x62, 0xd3, 0xca, 0x84, 0xa4, 0x15, 0x6a, 0x29, 0x33, 0xcc, 0x52, 0xa3,
	0x7e, 0x46, 0x98, 0x59, 0xc1, 0xa9, 0x45, 0x65, 0x99, 0xc9, 0xa9, 0x42, 0xb6, 0x5c, 0x3c, 0xce,
	0x45, 0xa9, 0x89, 0x25, 0xa9, 0x10, 0x61, 0x21, 0x11, 0x88, 0x3f, 0xf4, 0x50, 0x6c, 0x94, 0x12,
	0x45, 0x13, 0x85, 0xf8, 0x49, 0x89, 0x41, 0xc8, 0x9e, 0x8b, 0xcf, 0x2d, 0x33, 0x2f, 0x05, 0x22,
	0xee, 0x54, 0xe9, 0xe9, 0x42, 0xa2, 0x01, 0x49, 0x6c, 0x60, 0x71, 0x63, 0x40, 0x00, 0x00, 0x00,
	0xff, 0xff, 0x24, 0xd5, 0xd0, 0xc7, 0x46, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// AuthorServiceClient is the client API for AuthorService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type AuthorServiceClient interface {
	CreateAuthor(ctx context.Context, in *AuthorRequest, opts ...grpc.CallOption) (*AuthorResponse, error)
	FindAuthorByID(ctx context.Context, in *AuthorRequest, opts ...grpc.CallOption) (*AuthorResponse, error)
}

type authorServiceClient struct {
	cc *grpc.ClientConn
}

func NewAuthorServiceClient(cc *grpc.ClientConn) AuthorServiceClient {
	return &authorServiceClient{cc}
}

func (c *authorServiceClient) CreateAuthor(ctx context.Context, in *AuthorRequest, opts ...grpc.CallOption) (*AuthorResponse, error) {
	out := new(AuthorResponse)
	err := c.cc.Invoke(ctx, "/proto.AuthorService/CreateAuthor", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *authorServiceClient) FindAuthorByID(ctx context.Context, in *AuthorRequest, opts ...grpc.CallOption) (*AuthorResponse, error) {
	out := new(AuthorResponse)
	err := c.cc.Invoke(ctx, "/proto.AuthorService/FindAuthorByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AuthorServiceServer is the server API for AuthorService service.
type AuthorServiceServer interface {
	CreateAuthor(context.Context, *AuthorRequest) (*AuthorResponse, error)
	FindAuthorByID(context.Context, *AuthorRequest) (*AuthorResponse, error)
}

// UnimplementedAuthorServiceServer can be embedded to have forward compatible implementations.
type UnimplementedAuthorServiceServer struct {
}

func (*UnimplementedAuthorServiceServer) CreateAuthor(ctx context.Context, req *AuthorRequest) (*AuthorResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateAuthor not implemented")
}
func (*UnimplementedAuthorServiceServer) FindAuthorByID(ctx context.Context, req *AuthorRequest) (*AuthorResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindAuthorByID not implemented")
}

func RegisterAuthorServiceServer(s *grpc.Server, srv AuthorServiceServer) {
	s.RegisterService(&_AuthorService_serviceDesc, srv)
}

func _AuthorService_CreateAuthor_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AuthorRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthorServiceServer).CreateAuthor(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.AuthorService/CreateAuthor",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthorServiceServer).CreateAuthor(ctx, req.(*AuthorRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _AuthorService_FindAuthorByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AuthorRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthorServiceServer).FindAuthorByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.AuthorService/FindAuthorByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthorServiceServer).FindAuthorByID(ctx, req.(*AuthorRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _AuthorService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.AuthorService",
	HandlerType: (*AuthorServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateAuthor",
			Handler:    _AuthorService_CreateAuthor_Handler,
		},
		{
			MethodName: "FindAuthorByID",
			Handler:    _AuthorService_FindAuthorByID_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "Author.proto",
}
