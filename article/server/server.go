package server

import (
	"article-service/client"
	"article-service/config"
	pb "article-service/proto"
	"article-service/services"
	"context"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc/codes"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

type server struct {
	service *services.ArticleService
	config  config.Config
}

func New() pb.ArticleServiceServer {
	return &server{
		service: services.NewArticleService(),
		config:  config.Load("dev"),
	}
}

func (s *server) CreateArticle(ctx context.Context, req *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	// validate via author service
	grpc := client.ConnectAuthorServer(s.config.AuthorAddr)
	author, err := grpc.FindAuthorByID(context.Background(), &pb.AuthorRequest{Id: req.GetUserId()})
	if err != nil {
		return nil, err
	}

	if author.Id == "" {
		return nil, errors.New("no author found")
	}

	uid, err := uuid.FromString(req.GetUserId())
	if err != nil {
		return nil, err
	}

	result, err := s.service.CreateArticle(req.GetName(), req.GetBody())
	if err != nil {
		return nil, err
	}

	err = s.service.AddAuthor(result.ID, uid)
	if err != nil {
		return nil, err
	}

	return &pb.ArticleResponse{
		Id:        result.ID.String(),
		Name:      result.Name,
		Body:      result.Body,
		UserId:    uid.String(),
		Status:    result.Status,
		Slug:      result.Slug,
		CreatedAt: result.CreatedAt,
		UpdatedAt: result.UpdatedAt,
	}, nil
}

func (s *server) FindArticleByID(ctx context.Context, req *pb.ArticleRequest) (*pb.ArticleResponse, error) {
	result, err := s.service.FindArticleByID(req.GetId())
	if err != nil {
		return nil, err
	}

	return &pb.ArticleResponse{
		Id:        req.GetId(),
		Name:      result.Name,
		Body:      result.Body,
		UserId:    result.UserID.String(),
		Status:    result.Status,
		Slug:      result.Slug,
		CreatedAt: result.CreatedAt,
		UpdatedAt: result.UpdatedAt,
	}, nil
}

func (s *server) GetAll(ctx context.Context, _ *empty.Empty) (*pb.ArticleListResponse, error) {
	var list []*pb.ArticleResponse

	result, err := s.service.GetAllArticles()
	if err != nil {
		return nil, err
	}

	for _, article := range result {
		list = append(list, &pb.ArticleResponse{
			Id:        article.ID.String(),
			Name:      article.Name,
			Body:      article.Body,
			UserId:    article.UserID.String(),
			Status:    article.Status,
			Slug:      article.Slug,
			CreatedAt: article.CreatedAt,
			UpdatedAt: article.UpdatedAt,
		})
	}

	return &pb.ArticleListResponse{
		ArticleList: list,
	}, nil
}

func (s *server) FindArticlesByAuthorID(ctx context.Context, req *pb.ArticleRequest) (*pb.ArticleListResponse, error) {
	id, err := uuid.FromString(req.UserId)
	if err != nil {
		log.Println("error", err)
		return nil, status.Errorf(codes.InvalidArgument, "failed to parse id")
	}

	articles, err := s.service.FindArticlesByAuthorID(id)
	if err != nil {
		log.Println("error", err)
		return nil, status.Errorf(codes.Internal, "failed to find article")
	}

	res := &pb.ArticleListResponse{}
	for _, a := range articles {
		ar := &pb.ArticleResponse{}

		ar.UserId = a.UserID.String()
		ar.Body = a.Body
		ar.Id = a.ID.String()
		ar.Name = a.Name
		ar.Slug = a.Slug
		ar.CreatedAt = a.CreatedAt
		ar.UpdatedAt = a.UpdatedAt
		ar.Status = a.Status

		res.ArticleList = append(res.ArticleList, ar)
	}

	return res, nil
}

func Run(config config.Config) {
	listen, err := net.Listen("tcp", config.ArticleAddr)
	if err != nil {
		log.Fatalf("ERROR %s", err)
	}

	server := grpc.NewServer()
	reflection.Register(server)
	articleServer := New()
	pb.RegisterArticleServiceServer(server, articleServer)

	fmt.Println("server is on ", config.ArticleAddr)

	log.Fatalf("err: %s", server.Serve(listen))
}
