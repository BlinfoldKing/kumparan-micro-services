package storage

import (
	"article-service/domain"
	"time"

	uuid "github.com/satori/go.uuid"
)

// ArticleStorage is article storage in memory
type ArticleStorage struct {
	ArticleMap []domain.Article
}

var id1, _ = uuid.FromString("00000000-0000-0000-0000-000000000001")
var id2, _ = uuid.FromString("00000000-0000-0000-0000-000000000002")
var id3, _ = uuid.FromString("00000000-0000-0000-0000-000000000003")
var userID1, _ = uuid.FromString("00000000-0000-0000-0000-000000000001")
var userID2, _ = uuid.FromString("00000000-0000-0000-0000-000000000002")

// NewArticleStorage is to create article storage
func NewArticleStorage() *ArticleStorage {
	return &ArticleStorage{
		ArticleMap: []domain.Article{
			domain.Article{
				ID:        id1,
				Name:      "olahraga memmbuat badan bugar",
				Slug:      "olahraga-memmbuat-badan-bugar",
				UserID:    userID1,
				CreatedAt: time.Now().UnixNano(),
				UpdatedAt: time.Now().UnixNano(),
			},
			domain.Article{
				ID:        id2,
				Name:      "sarapan di pagi hari menyehatkan",
				Slug:      "sarapan-di-pagi-hari",
				UserID:    userID2,
				CreatedAt: time.Now().UnixNano(),
				UpdatedAt: time.Now().UnixNano(),
			},
			domain.Article{
				ID:        id3,
				Name:      "tidur awal menyehatkan",
				Slug:      "tidur-awal-menyehatkan",
				UserID:    userID1,
				CreatedAt: time.Now().UnixNano(),
				UpdatedAt: time.Now().UnixNano(),
			},
		},
	}
}
