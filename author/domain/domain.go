package domain

import uuid "github.com/satori/go.uuid"

// Author wrapper
type Author struct {
	ID    uuid.UUID
	Name  string
	Email string
}
