package console

import (
	"fmt"
	"log"
	"net"

	"github.com/spf13/cobra"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/container"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func init() {
	rootCmd.AddCommand(server)
}

var server = &cobra.Command{
	Use:   "server",
	Short: "start server, default is :3000",
	Run:   run,
}

func run(c *cobra.Command, args []string) {
	ctr := container.Container{}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", ctr.Config().Port))
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Listening on", ctr.Config().Port)

	server := grpc.NewServer()
	reflection.Register(server)
	proto.RegisterAuthorServiceServer(server, ctr.AuthorServiceServer())

	if err := server.Serve(lis); err != nil {
		log.Fatal("failed to server: ", err)
	}
}
