package server

import (
	"context"
	"log"

	uuid "github.com/satori/go.uuid"
	"google.golang.org/grpc/codes"

	"google.golang.org/grpc/status"

	"gitlab.com/BlinfoldKing/kumparan-micro-services/authorsvc"
	pb "gitlab.com/BlinfoldKing/kumparan-micro-services/proto"
)

type authorServiceServer struct {
	authorService authorsvc.AuthorService
}

func (a *authorServiceServer) CreateAuthor(ctx context.Context, req *pb.AuthorRequest) (*pb.AuthorResponse, error) {
	name := req.Name
	email := req.Email
	res := &pb.AuthorResponse{}

	author, err := a.authorService.Save(name, email)
	if err != nil {
		log.Println("error: ", err)
		return res, status.Error(codes.Internal, "failed to save")
	}

	res.Name = author.Name
	res.Email = author.Email
	res.Id = author.ID.String()

	return res, nil
}

func (a *authorServiceServer) FindAuthorByID(ctx context.Context, req *pb.AuthorRequest) (*pb.AuthorResponse, error) {
	res := &pb.AuthorResponse{}

	id, err := uuid.FromString(req.Id)
	if err != nil {
		log.Println("error: ", err)
		return res, status.Error(codes.Internal, "failed to parse id")
	}

	author, err := a.authorService.FindByID(id)
	if err != nil {
		log.Println("error: ", err)
		return res, status.Error(codes.Internal, "failed to find author")
	}

	res.Id = author.ID.String()
	if author.Email == "" {
		res.Id = ""
	}
	res.Name = author.Name
	res.Email = author.Email

	return res, nil
}

func NewAuthorServiceServer(authorService authorsvc.AuthorService) pb.AuthorServiceServer {
	return &authorServiceServer{
		authorService: authorService,
	}
}
