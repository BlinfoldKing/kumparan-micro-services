package authorsvc

import (
	"regexp"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/domain"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/repository"
)

type authorService struct {
	Mutation repository.AuthorMutation
	Query    repository.AuthorQuery
}

// NewAuthorService return authorService object
func NewAuthorService(mutation repository.AuthorMutation, query repository.AuthorQuery) AuthorService {
	return &authorService{
		Mutation: mutation,
		Query:    query,
	}
}

// FindByID find an author by it's id
func (a *authorService) FindByID(uuid uuid.UUID) (domain.Author, error) {
	result := <-a.Query.FindByID(uuid)
	if result.Error != nil {
		return domain.Author{}, errors.Wrap(result.Error, "FindByID")
	}

	return result.Result.(domain.Author), nil
}

// MaxEmailLen maximum characters allowed in email address length
// ref: http://www.dominicsayers.com/isemail/
const MaxEmailLen = 254

func validateEmail(email string) bool {
	// pattern for validating email
	// https://www.w3.org/TR/2016/REC-html51-20161101/sec-forms.html#email-state-typeemail
	pattern := "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
	rxEmail := regexp.MustCompile(pattern)
	if len(email) > MaxEmailLen || !rxEmail.MatchString(email) {
		return false
	}

	return true
}

// Save create & save new author
func (a *authorService) Save(name, email string) (domain.Author, error) {
	validEmail := validateEmail(email)
	if !validEmail {
		return domain.Author{}, errors.New("invalid email")
	}

	author := domain.Author{
		Name:  name,
		Email: email,
	}

	err := <-a.Mutation.Save(&author)
	if err != nil {
		return author, errors.Wrap(err, "Repository.Save")
	}

	return author, nil
}
