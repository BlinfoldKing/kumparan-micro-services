package authorsvc

import (
	"log"
	"sync"
	"testing"

	"gitlab.com/BlinfoldKing/kumparan-micro-services/repository"

	uuid "github.com/satori/go.uuid"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/domain"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/repository/mock"
)

func TestAuthorService_Save(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Run("success save new author", func(t *testing.T) {
		mutation := mock.NewMockAuthorMutation(ctrl)
		service := NewAuthorService(mutation, nil)

		author := domain.Author{
			Name:  "john",
			Email: "john@doe.com",
		}

		errChan := make(chan error)
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			errChan <- nil
			wg.Done()
		}()

		mutation.EXPECT().Save(&author).Return(errChan)
		want := author
		got, err := service.Save("john", "john@doe.com")

		assert.NoError(t, err)
		assert.Equal(t, want.Name, got.Name)
		assert.Equal(t, want.Email, got.Email)
		assert.NotEmpty(t, want.ID, got.ID)
	})

	t.Run("failed to save author when save got error", func(t *testing.T) {
		mutation := mock.NewMockAuthorMutation(ctrl)
		service := NewAuthorService(mutation, nil)

		author := domain.Author{
			// ID:    id,
			Name:  "john",
			Email: "john@doe.com",
		}

		errChan := make(chan error)
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			errChan <- errors.New("some error")
			wg.Done()
		}()

		mutation.EXPECT().Save(&author).Return(errChan)
		_, err := service.Save("john", "john@doe.com")
		assert.Error(t, err)
	})

	wrongEmail := []string{"miun.com", "miun", "", ".miun", "askjdnasdkajsndjabdhbajwhbdahwbdkjahwbdjahbwdjbhawjdbajhbwdjawhbdjahbwdjhabwhdjbwadhjbawjhbdajwhbdajwbdjhabwdjahbwd"}
	for _, e := range wrongEmail {
		t.Run("failed to save, if email invalid", func(t *testing.T) {
			valid := validateEmail(e)
			log.Println(valid)
			assert.Equal(t, false, valid)
		})
	}
}

func TestAuthorService_FindByID(t *testing.T) {
	ctrl := gomock.NewController(t)
	t.Run("failed to find author by id, should return error", func(t *testing.T) {
		query := mock.NewMockAuthorQuery(ctrl)
		service := NewAuthorService(nil, query)

		id := uuid.NewV4()
		res := make(chan repository.QueryResult)
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			res <- repository.QueryResult{
				Error: errors.New("some error"),
			}
			wg.Done()
		}()

		query.EXPECT().FindByID(id).Return(res)
		_, err := service.FindByID(id)

		assert.Error(t, err)
	})

	t.Run("success find author by id", func(t *testing.T) {
		query := mock.NewMockAuthorQuery(ctrl)
		service := NewAuthorService(nil, query)

		id := uuid.NewV4()
		author := domain.Author{
			ID:    id,
			Name:  "john",
			Email: "john@doe.com",
		}

		res := make(chan repository.QueryResult)
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			res <- repository.QueryResult{
				Error:  nil,
				Result: author,
			}
			wg.Done()
		}()

		query.EXPECT().FindByID(id).Return(res)
		got, _ := service.FindByID(id)
		want := author

		assert.Equal(t, want, got)
	})

	t.Run("get empty author, when author not found", func(t *testing.T) {
		want := domain.Author{}

		query := mock.NewMockAuthorQuery(ctrl)
		service := NewAuthorService(nil, query)

		id := uuid.NewV4()
		res := make(chan repository.QueryResult)
		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			res <- repository.QueryResult{
				Error:  nil,
				Result: want,
			}
			wg.Done()
		}()

		query.EXPECT().FindByID(id).Return(res)
		got, _ := service.FindByID(id)

		assert.Equal(t, want, got)

	})
}
