package authorsvc

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/domain"
)

type (
	// AuthorService service interface for author
	AuthorService interface {
		Save(name, email string) (domain.Author, error)
		FindByID(uuid uuid.UUID) (domain.Author, error)
	}
)
