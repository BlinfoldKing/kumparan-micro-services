package storage

import "gitlab.com/BlinfoldKing/kumparan-micro-services/domain"

// AuthorStorage is to create author
type AuthorStorage struct {
	AuthorMap []domain.Author
}

// NewAuthorStorage inmemory storage for author
func NewAuthorStorage(seeds []domain.Author) *AuthorStorage {
	storage := &AuthorStorage{}
	if len(seeds) > 0 {
		storage.AuthorMap = seeds
	}

	return storage
}
