package repository

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/domain"
)

// QueryResult hold the result of query operation
type QueryResult struct {
	Result interface{}
	Error  error
}

// AuthorQuery interface for query author's data
type AuthorQuery interface {
	FindByID(id uuid.UUID) <-chan QueryResult
}

// AuthorMutation interface for save & update author's data
type AuthorMutation interface {
	Save(author *domain.Author) <-chan error
}
