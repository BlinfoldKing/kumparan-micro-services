package inmemory

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/domain"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/repository"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/storage"
)

type authorQuery struct {
	Storage *storage.AuthorStorage
}

// NewAuthorQueryInMemory return  AuthorQueryInMemory
func NewAuthorQueryInMemory(strg *storage.AuthorStorage) repository.AuthorQuery {
	return &authorQuery{
		Storage: strg,
	}
}

// FindByID find author using it's ID
func (a *authorQuery) FindByID(id uuid.UUID) <-chan repository.QueryResult {
	result := make(chan repository.QueryResult)

	go func() {
		found := domain.Author{}
		for _, author := range a.Storage.AuthorMap {
			if author.ID == id {
				found = author
				break
			}
		}

		result <- repository.QueryResult{Result: found, Error: nil}
		close(result)
	}()

	return result
}
