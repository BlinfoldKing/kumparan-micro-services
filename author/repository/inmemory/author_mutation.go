package inmemory

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/domain"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/repository"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/storage"
)

// inmemeory implementation of AuthorMutation
type authorMutation struct {
	Storage *storage.AuthorStorage
}

// NewAuthorMutationInMemory return authorMutation object
func NewAuthorMutationInMemory(storage *storage.AuthorStorage) repository.AuthorMutation {
	return &authorMutation{
		storage,
	}
}

func (ar *authorMutation) Save(author *domain.Author) <-chan error {
	result := make(chan error)

	go func() {
		author.ID = uuid.NewV4()
		ar.Storage.AuthorMap = append(ar.Storage.AuthorMap, *author)
		result <- nil
		close(result)
	}()

	return result
}
