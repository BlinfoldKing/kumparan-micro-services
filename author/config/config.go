package config

import (
	"os"
	"strconv"

	"github.com/pkg/errors"
)

type Config struct {
	Port int
}

func (c *Config) Load() error {
	c.Port = 3000
	var err error
	port := os.Getenv("PORT")
	if port != "" {
		c.Port, err = strconv.Atoi(port)
		if err != nil {
			return errors.Wrap(err, "pase port")
		}
	}

	return nil
}
