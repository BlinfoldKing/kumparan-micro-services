# Author Service

Using the kumgo-stack style of code structure with some modification

## Feature
- start grpc server: `PORT=9000 author server`

## Testing
### Grpc
- Create Author
    ```
    grpcurl -plaintext -d '{"name": "miun", "email": "miun@email.com", "id": ""}' \
    localhost:8080 \
    proto.AuthorService/CreateAuthor
    ```
    
    result
    ```
    {
        "id": "f84d0786-1e81-4975-904b-3c8932c1e83f",
        "name": "miun",
        "email": "miun@email.com"
    }
    ```

- FindByID
    ```
    grpcurl -plaintext -d '{"name": "", "email": "", "id": "f84d0786-1e81-4975-904b-3c8932c1e83f"}' \
    localhost:8080 \
    proto.AuthorService/FindAuthorByID
    ```
    
    result
    ```
    {
    "id": "f84d0786-1e81-4975-904b-3c8932c1e83f",
    "name": "miun",
    "email": "miun@email.com"
    }
    ```

## Notes
- repository contains mutation & query interfaces
    - mutation used for save & update the domain data
    - the previous name choosen is repository, but it replaced by mutation to make it not confuse with repository package
    - query used to query domain data
    - the interfaces is placed in a repository.go file to make it easy to generate mock using gomock by using this command
        `mockgen -source=./repository/repository.go -destination=./repository/mock_repository.go -package=repository`