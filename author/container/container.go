package container

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/authorsvc"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/config"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/domain"
	pb "gitlab.com/BlinfoldKing/kumparan-micro-services/proto"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/repository"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/repository/inmemory"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/server"
	"gitlab.com/BlinfoldKing/kumparan-micro-services/storage"
)

type Container struct {
	cfg            *config.Config
	authorMutation repository.AuthorMutation
	authorQuery    repository.AuthorQuery

	authorStorage *storage.AuthorStorage

	authorService       authorsvc.AuthorService
	authorServiceServer pb.AuthorServiceServer
}

func (c *Container) AuthorStorage() *storage.AuthorStorage {
	id1, _ := uuid.FromString("00000000-0000-0000-0000-000000000001")
	id2, _ := uuid.FromString("00000000-0000-0000-0000-000000000002")
	seeds := []domain.Author{
		domain.Author{
			ID:    id1,
			Name:  "John",
			Email: "john@email.com",
		},
		domain.Author{
			ID:    id2,
			Name:  "Jean",
			Email: "jean@email.com",
		},
	}

	if c.authorStorage == nil {
		c.authorStorage = storage.NewAuthorStorage(seeds)
	}

	return c.authorStorage
}

func (c *Container) AuthorMutation() repository.AuthorMutation {
	if c.authorMutation == nil {
		c.authorMutation = inmemory.NewAuthorMutationInMemory(c.AuthorStorage())
	}

	return c.authorMutation
}

func (c *Container) AuthorQuery() repository.AuthorQuery {
	if c.authorQuery == nil {
		c.authorQuery = inmemory.NewAuthorQueryInMemory(c.AuthorStorage())
	}
	return c.authorQuery
}

func (c *Container) AuthorService() authorsvc.AuthorService {
	if c.authorService == nil {
		c.authorService = authorsvc.NewAuthorService(c.AuthorMutation(), c.AuthorQuery())
	}

	return c.authorService
}

func (c *Container) AuthorServiceServer() pb.AuthorServiceServer {
	if c.authorServiceServer == nil {
		c.authorServiceServer = server.NewAuthorServiceServer(c.AuthorService())
	}

	return c.authorServiceServer
}

func (c *Container) Config() config.Config {
	if c.cfg == nil {
		c.cfg = &config.Config{}
		c.cfg.Load()
	}

	return *c.cfg
}
